@echo on

set TEST_DATA_FOLDER=.\
set BIN_FOLDER=..\src\Release\

set MY_EXE=%BIN_FOLDER%NeedleApp.exe
set KOT_EXE=%TEST_DATA_FOLDER%/kotovsky/haystack_and_needle.exe

goto :SHORT_TEST

rem =======================================================================================================================
:SHORT_TEST

set NEEDLE_PATH=F:\Documents\Self-Education\Perfect code\NeedleInHaystack\NeedleInHaystack\src\NeedleTest\TestFiles\TestStrategy\several_sequence_entries_needle.txt
set HAYSTECK_PATH=F:\Documents\Self-Education\Perfect code\NeedleInHaystack\NeedleInHaystack\src\NeedleTest\TestFiles\TestStrategy\several_sequence_entries_haystack.txt
set THRESHOLD=3


call "%MY_EXE%" "%HAYSTECK_PATH%" "%NEEDLE_PATH%" "%THRESHOLD%" > logs/my_several_sequence_entries_needle_3.log
call "%KOT_EXE%" "%HAYSTECK_PATH%" "%NEEDLE_PATH%" "%THRESHOLD%" > logs/kot_several_sequence_entries_needle_3.log


if ERRORLEVEL 1 goto :ERROR

goto :END

rem =======================================================================================================================
:LONG_TEST

set NEEDLE_PATH=F:\Documents\Self-Education\Perfect code\NeedleInHaystack\NeedleInHaystack\manual_tests\msvproc.dll
set HAYSTECK_PATH=C:\WMs\Windows 10 32-bit\Windows 10 32-bit-000002.vmdk
set THRESHOLD=1000

rem call "%MY_EXE%" "%HAYSTECK_PATH%" "%NEEDLE_PATH%" "%THRESHOLD%" > logs/long_test_myapp3.log
call "%KOT_EXE%" "%HAYSTECK_PATH%" "%NEEDLE_PATH%" "%THRESHOLD%" > logs/long_test_kot3.log
if ERRORLEVEL 1 goto :ERROR


rem =======================================================================================================================
:END
exit

:ERROR
echo [WARNING] - See errors above!
exit

rem "F:\Elcomsoft\old\Sources\Externals\ext.zip" "F:\Elcomsoft\old\Sources\Externals\python27.dll" 2459647
rem "F:\Documents\Self-Education\Perfect code\NeedleInHaystack\NeedleInHaystack\test_data\test_spec_haystack.txt" "F:\Documents\Self-Education\Perfect code\NeedleInHaystack\NeedleInHaystack\test_data\test_spec_needle.txt" 3
rem "F:\Documents\Self-Education\Perfect code\NeedleInHaystack\NeedleInHaystack\test_data\test_end_of_file_haystack.txt" "F:\Documents\Self-Education\Perfect code\NeedleInHaystack\NeedleInHaystack\test_data\test_end_of_file_needle.txt" 3