#include "stdafx.h"
#include "NeedleInHaystack.h"

using namespace NeedleApp;

std::unique_ptr<needle::Callbacks> CreateCallbacks(const std::wstring& haystackPath, const std::wstring& needlePath)
{
    std::unique_ptr<needle::Callbacks> context(new needle::Callbacks());

    //Handle sequence found
    needle::NeedleFoundHandler sequenceInfo = [&haystackPath, &needlePath](size_t needleSize, int64_t haystackOffset, size_t needleOffset)
    {
        std::wstringstream message;
        message << L"sequence of length = "     << needleSize       << L" ";
        message << L"found at haystack offset " << haystackOffset   << L", ";
        message << L"needle offset "            << needleOffset     << L"";

        Logger::Log(message.str());
    };

    context->SetFoundHandler(sequenceInfo);

    //Handle progress text
    context->SetProgressTextHandler([](const std::wstring& text)
    {
        Logger::Log(text, true);
    });

    //Percents
    context->SetProgressHandler([](size_t percent)
    {
        std::wstringstream message;
        message << L"Progress = " << percent;

        Logger::Log(message.str());
    });

    return context;
}

needle::Context CreateContext(int argc, _TCHAR* argv[])
{
    try
    {
        if(argc >= 4)
        {
            std::wstring haystackPath = argv[1];
            std::wstring needle = argv[2];
            size_t sequenceSize = std::stoi(argv[3]);

            return needle::Context(haystackPath, needle, sequenceSize, CreateCallbacks(haystackPath, needle));
        }
    }
    catch(const std::exception& ex)
    {
        throw Exceptions::ArgumentParseException(ex);
    }

    throw Exceptions::ArgumentParseException();
}

void ShowError(const std::string& message)
{
    Logger::Log("[ERROR]: " + message);
}

void ShowError(const std::exception& ex)
{
    ShowError(ex.what());
}

void ShowHelp()
{
    Logger::Log(L"1 param - haystack");
    Logger::Log(L"2 param - needle");
    Logger::Log(L"3 param - threshold");
}

int _tmain(int argc, _TCHAR* argv[])
{
    try
    {
        auto context = CreateContext(argc, argv);

        Logger::Log(L"Haystack path:\t" + context.GetHaystackPath());
        Logger::Log(L"Needle path:\t"   + context.GetNeedlePath());

        needle::FindNeedle(context);
        Logger::Log(L"Done\n", true);
    }
    catch (const Exceptions::ArgumentParseException&)
    {
        ShowHelp();
        return 1;
    }
    catch (const std::invalid_argument& ex)
    {
        ShowError(ex);
        return 1;
    }
    catch (const std::exception& ex)
    {
        ShowError(ex);
        return 1;
    }
    catch (...)
    {
        ShowError("Unknown error");
        return 1;
    }

    return 0;
}

