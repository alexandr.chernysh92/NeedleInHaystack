#pragma once

namespace NeedleApp
{
    class Logger
    {
    public:
        static void Log(const std::string& message, bool timestamp = false);
        static void Log(const std::wstring& message, bool timestamp = false);
        static void Log(const wchar_t* message, bool timestamp = false);

    private:
        Logger();
    };
}