// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <stdint.h>
#include <string>
#include <memory>
#include <sstream>
#include <ctime>
#include <chrono>
#include <thread>

#include "Logger.h"
#include "Exceptions.h"
#include "Callbacks.h"
#include "Context.h"