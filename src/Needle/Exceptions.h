#pragma once

namespace NeedleApp
{
    namespace Exceptions
    {
        class ArgumentParseException: public std::exception
        {
            public:
                ArgumentParseException(): std::exception("Cannot parse arguments"){}
                ArgumentParseException(const std::exception& ex): std::exception(ex){}
        };
    }
}