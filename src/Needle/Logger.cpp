#include "stdafx.h"
#include "Logger.h"

using namespace NeedleApp;

namespace
{
    std::string GetTimestamp()
    {
        time_t rawtime;
        time(&rawtime);

        tm timeinfo;
        localtime_s(&timeinfo, &rawtime);

        char buffer[80];
        strftime(buffer, sizeof(buffer), "%d-%m-%Y %I:%M:%S", &timeinfo);
        return buffer;
    }
}

Logger::Logger(){}

void Logger::Log(const std::string& message, bool timestamp)
{
    Log(std::wstring(message.begin(), message.end()), timestamp);
}

void Logger::Log(const std::wstring& message, bool timestamp)
{
    Log(message.c_str(), timestamp);
}

void Logger::Log(const wchar_t* message, bool timestamp)
{
    if (timestamp)
    {
        std::string timestamp = GetTimestamp();
        std::wstring wTimestamp(timestamp.begin(), timestamp.end());

        std::wcout << "[" << wTimestamp.c_str() << "] ";
    }
    
    std::wcout << message << L"\n";
}