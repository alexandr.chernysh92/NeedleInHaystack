#pragma once
#include "CacheItem.h"

namespace needle
{
    class CacheItemComparator
    {
    public:
        CacheItemComparator();
        explicit CacheItemComparator(size_t threshold);

        bool operator()(const CacheItem& a, const CacheItem& b) const;

    private:
        const size_t threshold_;
    };

    typedef std::multiset<CacheItem, CacheItemComparator> CacheMap;
}