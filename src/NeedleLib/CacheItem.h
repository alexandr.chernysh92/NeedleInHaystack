#pragma once

namespace needle
{
    struct CacheItem
    {
        CacheItem(c_uchar* mem, size_t size);
        CacheItem(c_uchar* mem, size_t size, size_t index);

        c_uchar*    mem;
        size_t      size;
        int         index;
        bool        indexed;
    };

    typedef std::unique_ptr<CacheItem> CacheItemPtr;
}