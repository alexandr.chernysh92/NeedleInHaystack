#include "stdafx.h"
#include "CacheMap.h"

using namespace needle;

CacheItemComparator::CacheItemComparator()
    : threshold_(0) {}

CacheItemComparator::CacheItemComparator(size_t threshold)
    : threshold_(threshold) {}

bool CacheItemComparator::operator()(const CacheItem& lhs, const CacheItem& rhs) const
{
    const auto* lmem = lhs.mem;
    const auto* rmem = rhs.mem;

    if (*lmem != *rmem)
    {
        return *lmem < *rmem;
    }

    const auto minSize = std::min(lhs.size, rhs.size);
    auto diffResult = std::mismatch(lmem, lmem + minSize, rmem, rmem + minSize);

    if (static_cast<size_t>(diffResult.first - lmem) < threshold_)
    {
        //when this needle less than our threshold
        //so we just compare symbols
        return *diffResult.first < *diffResult.second;
    }
    else if (lhs.indexed && !rhs.indexed)
    {
        //when:
        // - we found match in pos greater than our threshold
        // - lhs item is from cache
        // - rhs is item for search
        //so we just say search needle item >= cached item
        return false;
    }
    else if (rhs.indexed && !lhs.indexed)
    {
        //Equality check:
        // - we found match in pos greater than our threshold
        // - lhs is item for search
        // - rhs item is from cache
        //so we just say cached item == search needle item

        //std::cout << "\nfound!\n";
        return false;
    }
    else
    {
        //When we just cache needle
        return lhs.size < rhs.size;
    }
}