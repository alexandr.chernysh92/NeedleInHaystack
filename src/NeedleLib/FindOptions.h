#pragma once

namespace Needle
{
    class FindOptions
    {
    public:
        FindOptions(const std::wstring& haystackPath, const std::wstring& needlePath, uint64_t threshold);

        std::wstring GetHaystackPath() const;
        std::wstring GetNeedlePath() const;
        uint64_t GetThreshold() const;

    private:
        std::wstring haystackPath_;
        std::wstring needlePath_;
        uint64_t threshold_;
    };
}