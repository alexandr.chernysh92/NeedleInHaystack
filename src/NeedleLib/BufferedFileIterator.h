#pragma once

namespace needle
{
    struct Range
    {
        Range();
        size_t GetDistance() const;

        uint64_t start;
        uint64_t end;
    };

    class BufferedFileIterator : public std::istream_iterator<c_uchar>
    {
    public:
        //constructors
        BufferedFileIterator(const std::wstring& path, uint64_t minRange);
        explicit BufferedFileIterator(const std::wstring& path);

        //iterator
        BufferedFileIterator& operator++();
        BufferedFileIterator& BufferedFileIterator::operator +=(int pos);
        bool operator!=(const BufferedFileIterator& rhs) const;
        c_uchar& operator*() const;

        size_t      GetBufferSize() const;
        uint64_t    GetCurrentPos();
    private:
        bool Move(uint64_t offset);

        bool SetRange(uint64_t start, uint64_t isEnd_);
        void UpdateInternalBuffer();
        void MoveFocus(uint64_t start);

    private:
        std::wstring path_;
        bool isEnd_;
        std::ifstream fileStream_;
        uchar_vt internalBuffer_;
        
        size_t capacity_ = 0;
        uint64_t fileSize_ = 0;

        Range fileRange_;
        Range focusRange_;
    };
}