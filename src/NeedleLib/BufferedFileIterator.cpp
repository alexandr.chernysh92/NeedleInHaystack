#include "stdafx.h"
#include "BufferedFileIterator.h"
#include "Utils.h"

using namespace needle;

namespace
{
    const static uint64_t INTERANL_CAPACITY = 10 * 1024 * 1024;
}

Range::Range() 
    : start(0)
    , end(0) {}

size_t Range::GetDistance() const
{
    return static_cast<size_t>(end - start);
}

BufferedFileIterator::BufferedFileIterator(const std::wstring& path, uint64_t minRange)
    : capacity_(INTERANL_CAPACITY)
    , isEnd_(false)
    , fileSize_(Utils::GetFileSize(path))
    , path_(path)
{
    fileStream_ = Utils::OpenFile(path);
    internalBuffer_.resize(capacity_);

    if (!SetRange(0, minRange))
    {
        throw std::runtime_error("cannot set range");
    }
}

BufferedFileIterator::BufferedFileIterator(const std::wstring& path)
    : capacity_(0)
    , isEnd_(true)
    , fileSize_(Utils::GetFileSize(path)) 
    , path_(path)
{
    fileRange_.end = fileSize_;
    fileRange_.start = fileSize_;
}

BufferedFileIterator& BufferedFileIterator::operator++()
{ 
    Move(1);
    return *this; 
}

c_uchar& BufferedFileIterator::operator*() const
{ 
    size_t bufferRangeStart = static_cast<size_t>(fileRange_.start - focusRange_.start);
    return internalBuffer_.at(bufferRangeStart);
}

bool BufferedFileIterator::operator!=(const BufferedFileIterator& rhs) const
{
    if (rhs.isEnd_ != this->isEnd_)
    {
        return true;
    }

    //todo: other checks
    return false;
}

BufferedFileIterator& BufferedFileIterator::operator+=(int pos)
{
    Move(pos);
    return *this;
}

size_t BufferedFileIterator::GetBufferSize() const
{
    return static_cast<size_t>(focusRange_.end - fileRange_.start);
}

uint64_t BufferedFileIterator::GetCurrentPos()
{
    return fileRange_.start;
}
bool BufferedFileIterator::Move(uint64_t offset)
{
    isEnd_ = !SetRange(fileRange_.start + offset, fileRange_.end + offset);
    return !isEnd_;
}

bool BufferedFileIterator::SetRange(uint64_t start, uint64_t endPos)
{
    if (start >= fileSize_ || endPos >= fileSize_ + 1)
    {
        return false;
    }

    if (start < fileRange_.start)
    {
        throw std::invalid_argument("cannot move range to back");
    }

    if (isEnd_)
    {
        return false;
    }


    fileRange_.start = start;
    fileRange_.end = endPos;

    UpdateInternalBuffer();

    return true;
}

void BufferedFileIterator::UpdateInternalBuffer()
{
    if (fileRange_.GetDistance() > capacity_)
    {
        capacity_ = fileRange_.GetDistance();
    }

    if (fileRange_.start >= focusRange_.end)
    {
        MoveFocus(fileRange_.start);
    }
    else if (fileRange_.end > focusRange_.end)
    {
        MoveFocus(fileRange_.start);
    }
}

void BufferedFileIterator::MoveFocus(uint64_t start)
{
    fileStream_.seekg(start);

    fileStream_.read(reinterpret_cast<char*>(&internalBuffer_.at(0)), capacity_);
    std::streamsize readBytes = fileStream_.gcount();

    focusRange_.start = start;
    focusRange_.end = start + readBytes;
}