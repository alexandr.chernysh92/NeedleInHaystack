#include "stdafx.h"
#include "Utils.h"

std::ifstream Utils::OpenFile(const std::wstring& path)
{
    std::ifstream file(path, std::ifstream::in | std::ifstream::binary);
    if (!file.is_open())
    {
        throw std::invalid_argument("Cannot open haystack file");
    }

    if (file.peek() == std::ifstream::traits_type::eof())
    {
        throw std::invalid_argument("Haystack is empty");
    }

    return file;
}

void Utils::FileToVector(const std::wstring& path, uchar_vt& buffer)
{
    auto fileStreem = Utils::OpenFile(path);

    size_t size = static_cast<size_t>(Utils::GetStreamSize(fileStreem));
    buffer.resize(size);
    
    fileStreem.read(reinterpret_cast<char*>(&buffer.at(0)), buffer.size());
}

void Utils::ReadFromFile(const std::wstring& path, uchar_vt& buffer, int64_t offset, size_t length)
{
    auto fileStreem = Utils::OpenFile(path);
    fileStreem.seekg(offset, fileStreem.end);
    
    buffer.resize(length);

    fileStreem.read(reinterpret_cast<char*>(&buffer.at(0)), buffer.size());
}

uint64_t Utils::GetFileSize(const std::wstring& path)
{
    std::ifstream stream = OpenFile(path);
    return GetStreamSize(stream);
}

uint64_t Utils::GetStreamSize(std::ifstream& stream)
{
    stream.seekg(0, stream.end);
    uint64_t length = static_cast<uint64_t>(stream.tellg());
    stream.seekg(0, stream.beg);

    return length;
}