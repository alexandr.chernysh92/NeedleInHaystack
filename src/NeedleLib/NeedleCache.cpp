#include "stdafx.h"
#include "Utils.h"
#include "CacheItem.h"
#include "NeedleCache.h"

using namespace needle;

NeedleCache::NeedleCache(size_t threshold)
    : threshold_(threshold){}

std::unique_ptr<NeedleCache> NeedleCache::Create(const std::wstring& needlePath, size_t threshold)
{
    std::unique_ptr<NeedleCache> cache(new NeedleCache(threshold));
    Utils::FileToVector(needlePath, cache->needleBuffer_);

    CacheItemComparator comparator(threshold);
    cache->cacheMap_.reset(new CacheMap(comparator));

    size_t bufferSize = cache->needleBuffer_.size();
    for (size_t index = 0; index < bufferSize; ++index)
    {
        CacheItem item(&cache->needleBuffer_[index], bufferSize - index, index);
        cache->cacheMap_->emplace(item);
    }

    return std::move(cache);
}

CacheItemPtr NeedleCache::Find(const CacheItem& item) const
{
    //Find range of needles
    auto rangeIter = cacheMap_->equal_range(item);
    if (rangeIter.first == cacheMap_->end() || rangeIter.first == rangeIter.second)
    {
        return CacheItemPtr();
    }

    //Calculate sizes of needles
    std::deque<CacheItemPtr> foundItems;
    for (auto& iter = rangeIter.first; iter != rangeIter.second; ++iter)
    {
        const auto minSize = std::min(item.size, iter->size);
        auto diffResult = std::mismatch(item.mem, item.mem + minSize, iter->mem, iter->mem + minSize);

        size_t size = std::distance(item.mem, diffResult.first);
        if (size >= threshold_)
        {
            CacheItemPtr calculatedItem(new CacheItem(iter->mem, size, iter->index));
            foundItems.push_back(std::move(calculatedItem));
        }
    }

    //Find max needle
    std::deque<CacheItemPtr>::iterator maxCache = std::max_element(foundItems.begin(), foundItems.end(), 
        [](const CacheItemPtr& l, const CacheItemPtr& r)
    {
        if (l->size < r->size)
        {
            return true;
        }
        if (l->size == r->size)
        {
            return l->index > r->index;
        }

        return false;
    });

    if (maxCache != foundItems.end())
    {
        return std::move(*maxCache);
    }
    else
    {
        return CacheItemPtr();
    }
}