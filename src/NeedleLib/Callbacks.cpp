#include "stdafx.h"
#include "Callbacks.h"

using namespace needle;

void Callbacks::NeedleFound(size_t needleSize, int64_t haystackOffset, size_t needleOffset) const
{
    if (sequenceFoundHandler_ != NULL)
    {
        sequenceFoundHandler_(needleSize, haystackOffset, needleOffset);
    }
}

void Callbacks::ChangeProgressText(const std::wstring& text) const
{
    if (progressTextChangedHanlder_ != NULL)
    {
        progressTextChangedHanlder_(text);
    }
}

void Callbacks::SetFoundHandler(NeedleFoundHandler handler)
{
    sequenceFoundHandler_ = handler;
}

void Callbacks::SetProgressHandler(ProgressChangedHanlder handler)
{
    progressChangedHanlder_ = handler;
}

void Callbacks::SetProgressTextHandler(ProgressTextChangedHanlder handler)
{
    progressTextChangedHanlder_ = handler;
}