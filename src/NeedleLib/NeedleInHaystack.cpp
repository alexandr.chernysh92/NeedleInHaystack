﻿#include "stdafx.h"
#include "NeedleInHaystack.h"
#include "Utils.h"
#include "BufferedFileIterator.h"
#include "NeedleCache.h"

using namespace needle;

namespace
{
    const bool VERIFY_FOUND_NEEDLE_INFO = false;

    bool VerifyNeedleInfo(const Context& context, size_t needleSize, int64_t haystackOffset, size_t needleOffset)
    {
        //Read bytes from needle file
        uchar_vt needleBuffer(needleSize);
        Utils::ReadFromFile(context.GetNeedlePath(), needleBuffer, needleOffset, needleSize);

        //Read bytes from haystack file
        uchar_vt haystackBuffer(needleSize);
        Utils::ReadFromFile(context.GetHaystackPath(), haystackBuffer, haystackOffset, needleSize);

        //Compare bytes from needle and haystack files
        auto diffResult = std::mismatch(needleBuffer.begin(), needleBuffer.end(), haystackBuffer.begin(), haystackBuffer.end());
        if (diffResult.first != needleBuffer.end())
        {
            return false;
        }

        return true;
    }

    void FindNeedlesInHaystack(
        NeedleFoundHandler foundNeedleFc,
        const NeedleCache* cache,
        BufferedFileIterator& haystackIter,
        BufferedFileIterator& haystackEnd)
    {
        while (haystackIter != haystackEnd)
        {
            CacheItem item(&(*haystackIter), haystackIter.GetBufferSize());

            auto iter = cache->Find(item);
            if (iter.get())
            {
                foundNeedleFc(iter->size, haystackIter.GetCurrentPos(), iter->index);
                std::advance(haystackIter, iter->size - 1);
            }

            ++haystackIter;
        }
    }
}

void needle::FindNeedle(const Context& context)
{
    //start caching of needle file
    context.GetCallbacks()->ChangeProgressText(L"Create cache");

    auto needleCache = NeedleCache::Create(
        context.GetNeedlePath(),
        context.GetThreshold()
    );
    
    //start search needle in haystack
    context.GetCallbacks()->ChangeProgressText(L"Search");

    BufferedFileIterator haystackStart(context.GetHaystackPath(), context.GetThreshold());
    BufferedFileIterator haystackend(context.GetHaystackPath());

    NeedleFoundHandler foundNeedleHandler = [&context](size_t needleSize, int64_t haystackOffset, size_t needleOffset)
    {
        if (VERIFY_FOUND_NEEDLE_INFO)
        {
            if (!VerifyNeedleInfo(context, needleSize, haystackOffset, needleOffset))
            {
                throw std::runtime_error("The needle found isn't valid");
            }
        }

        context.GetCallbacks()->NeedleFound(needleSize, haystackOffset, needleOffset);
    };

    FindNeedlesInHaystack(foundNeedleHandler, needleCache.get(), haystackStart, haystackend);
}