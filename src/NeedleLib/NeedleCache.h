#pragma once
#include "CacheItem.h"
#include "CacheMap.h"

namespace needle
{
    class NeedleCache
    {
    public:
        static std::unique_ptr<NeedleCache> Create(const std::wstring& needlePath, size_t threshold);
        CacheItemPtr Find(const CacheItem& item) const;

    private:
        explicit NeedleCache(size_t threshold);

    private:
        size_t threshold_;
        uchar_vt needleBuffer_;
        std::unique_ptr<CacheMap> cacheMap_;
    };
}