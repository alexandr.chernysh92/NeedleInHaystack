#pragma once
#include "stdafx.h"

namespace needle
{
    typedef std::function<void(size_t, int64_t, size_t)>    NeedleFoundHandler;
    typedef std::function<void (const std::wstring&)>       ProgressTextChangedHanlder;
    typedef std::function<void (size_t)>                    ProgressChangedHanlder;

    class Callbacks
    {
    public:
        void NeedleFound(size_t needleSize, int64_t haystackOffset, size_t needleOffset) const;
        void ChangeProgressText(const std::wstring& text) const;

        void SetFoundHandler(NeedleFoundHandler handler);
        void SetProgressHandler(ProgressChangedHanlder handler);
        void SetProgressTextHandler(ProgressTextChangedHanlder handler);

    private:
        NeedleFoundHandler sequenceFoundHandler_;
        ProgressChangedHanlder progressChangedHanlder_;
        ProgressTextChangedHanlder  progressTextChangedHanlder_;
    };
}