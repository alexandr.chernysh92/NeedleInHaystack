#pragma once

namespace Utils
{
    std::ifstream OpenFile(const std::wstring& path);
    void FileToVector(const std::wstring& path, uchar_vt& buffer);
    void ReadFromFile(const std::wstring& path, uchar_vt& buffer, int64_t offset, size_t length);
    uint64_t GetFileSize(const std::wstring& path);
    uint64_t GetStreamSize(std::ifstream& stream);
}

