// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <stdint.h>
#include <string>
#include <sstream>
#include <memory>
#include <vector>
#include <deque>
#include <fstream>
#include <functional>
#include <iterator>
#include <functional>
#include <algorithm>
#include <unordered_set>
#include <map>
#include <thread>
#include <mutex>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include <set>

typedef unsigned char uchar;
typedef const uchar c_uchar;
typedef std::vector<uchar> uchar_vt;

