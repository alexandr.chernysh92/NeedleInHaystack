#include "stdafx.h"
#include "CacheItem.h"

using namespace needle;

CacheItem::CacheItem(c_uchar* mem, size_t size)
    : mem(mem)
    , size(size)
    , index(-1)
    , indexed(false)
{}

CacheItem::CacheItem(c_uchar* mem, size_t size, size_t index)
    : mem(mem)
    , size(size)
    , index(index)
    , indexed(true)
{}