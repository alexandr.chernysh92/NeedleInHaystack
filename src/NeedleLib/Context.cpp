#include "stdafx.h"
#include "Context.h"

using namespace needle;

Context::Context(const std::wstring& haystackPath, const std::wstring& needlePath, size_t threshold, std::unique_ptr<Callbacks> callbacks)
    : haystackPath_(haystackPath)
    , needlePath_(needlePath)
    , threshold_(threshold)
    , callbacks_(std::move(callbacks))
{
    if (threshold == 0)
    {
        throw std::invalid_argument("threshold cannnot be 0");
    }
}

std::wstring Context::GetHaystackPath() const
{
    return haystackPath_;
}

std::wstring Context::GetNeedlePath() const
{
    return needlePath_;
}

size_t Context::GetThreshold() const
{
    return threshold_;
}

const Callbacks* Context::GetCallbacks() const
{
    return callbacks_.get();
}