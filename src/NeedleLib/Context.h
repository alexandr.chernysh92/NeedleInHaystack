#pragma once
#include "Callbacks.h"

namespace needle
{
    class Context
    {
    public:
        Context(const std::wstring& haystackPath, const std::wstring& needlePath, size_t threshold, std::unique_ptr<Callbacks> callbacks);

        std::wstring GetHaystackPath() const;
        std::wstring GetNeedlePath() const;
        size_t GetThreshold() const;
        const Callbacks* GetCallbacks() const;

    private:
        std::wstring                haystackPath_;
        std::wstring                needlePath_;
        size_t                      threshold_;
        std::unique_ptr<Callbacks>  callbacks_;
    };
}
