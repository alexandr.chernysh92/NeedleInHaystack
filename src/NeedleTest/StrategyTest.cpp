#include "stdafx.h"
#include "StrategyTest.h"
#include "NeedleInHaystack.h"
#include "FindOptions.h"
#include "Context.h"

using namespace needleTest;
using namespace needle;

namespace
{
    static const std::wstring PATH_TO_TEST_FILES = L"..\\NeedleTest\\TestFiles\\";
    static const bool SHOW_OUTPUT = false;

    class SequenceInfo
    {
    public:
        SequenceInfo() :Length(0), HaystackOffset(0), NeedleOffset(0) {}
        SequenceInfo(size_t needleSize, int64_t haystackOffset, size_t needleOffset)
            : Length(needleSize)
            , HaystackOffset(haystackOffset)
            , NeedleOffset(needleOffset) {}

        size_t Length;
        uint64_t HaystackOffset;
        uint64_t NeedleOffset;
    };

    std::string GetTimestamp()
    {
        time_t rawtime;
        time(&rawtime);

        tm timeinfo;
        localtime_s(&timeinfo, &rawtime);

        char buffer[80];
        strftime(buffer, sizeof(buffer), "%d-%m-%Y %I:%M:%S", &timeinfo);
        return buffer;
    }

    std::unique_ptr<Callbacks> CreateCallback(std::vector<SequenceInfo>& sequences, const std::wstring& haystackPath, const std::wstring& needlePath)
    {
        std::unique_ptr<Callbacks> calls(new Callbacks());

        calls->SetFoundHandler([&](size_t needleSize, int64_t haystackOffset, size_t needleOffset)
        {
            if (SHOW_OUTPUT)
            {
                std::wstringstream message;
                std::wcout << L"sequence of length = "      << needleSize       << L" ";
                std::wcout << L"found at haystack offset "  << haystackOffset   << L", ";
                std::wcout << L"needle offset "             << needleOffset     << L"\n";
            }
            
            sequences.push_back(SequenceInfo(needleSize, haystackOffset, needleOffset));
        });

        calls->SetProgressTextHandler([&](const std::wstring& text)
        {
            if (SHOW_OUTPUT)
            {
                std::wcout << "\n" << GetTimestamp().c_str();
                std::wcout << " " << text.c_str() << "\n";
            }
        });

        return calls;
    }

    std::vector<SequenceInfo> FindNeedle(const std::wstring& haystackPath, const std::wstring& needlePath, uint64_t threshold)
    {
        std::vector<SequenceInfo> sequences;
        auto calls = CreateCallback(sequences, haystackPath, needlePath);
        
        Context context(haystackPath, needlePath, threshold, std::move(calls));
        needle::FindNeedle(context);

        return sequences;
    }
}

void StrategyTest::TestBeginOfFile()
{
    std::vector<SequenceInfo> sequences = FindNeedle(
        PATH_TO_TEST_FILES + L"TestData\\test_begin_of_file_haystack.txt",
        PATH_TO_TEST_FILES + L"TestData\\test_begin_of_file_needle.txt",
        4);

    CPPUNIT_ASSERT(sequences.size() == 1);

    CPPUNIT_ASSERT(sequences[0].Length == 4);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 0);
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 0);
}

void StrategyTest::TestEndOfFile()
{
    std::vector<SequenceInfo> sequences = FindNeedle(
        PATH_TO_TEST_FILES + L"TestData\\test_end_of_file_haystack.txt",
        PATH_TO_TEST_FILES + L"TestData\\test_end_of_file_needle.txt",
        3);

    CPPUNIT_ASSERT(sequences.size() == 1);

    CPPUNIT_ASSERT(sequences[0].Length == 3);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 50);
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 19);
}

void StrategyTest::TestFullNeedleSequence()
{
    std::vector<SequenceInfo> sequences = FindNeedle(
        PATH_TO_TEST_FILES + L"TestData\\test_full_needle_sequence_haystack.txt",
        PATH_TO_TEST_FILES + L"TestData\\test_full_needle_sequence_needle.txt",
        54);

    CPPUNIT_ASSERT(sequences.size() == 1);

    CPPUNIT_ASSERT(sequences[0].Length == 54);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 9);
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 0);
}

void StrategyTest::TestSecondEntry()
{
    std::vector<SequenceInfo> sequences = FindNeedle(
        PATH_TO_TEST_FILES + L"TestData\\test_second_entry_haystack.txt",
        PATH_TO_TEST_FILES + L"TestData\\test_second_entry_needle.txt",
        4);

    CPPUNIT_ASSERT(sequences.size() == 1);

    CPPUNIT_ASSERT(sequences[0].Length == 9);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 9); 
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 4);
}

void StrategyTest::TestMiddleEntry()
{
    std::vector<SequenceInfo> sequences = FindNeedle(
        PATH_TO_TEST_FILES + L"TestData\\test_middle_haystack.txt",
        PATH_TO_TEST_FILES + L"TestData\\test_middle_needle.txt",
        5);

    CPPUNIT_ASSERT(sequences.size() == 1);

    CPPUNIT_ASSERT(sequences[0].Length == 6); 
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 13); 
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 3); 
}

void StrategyTest::TestSpecificaton()
{
    std::vector<SequenceInfo> sequences = FindNeedle(
        PATH_TO_TEST_FILES + L"TestData\\test_spec_haystack.txt",
        PATH_TO_TEST_FILES + L"TestData\\test_spec_needle.txt",
        3);

    CPPUNIT_ASSERT(sequences.size() == 3);

    CPPUNIT_ASSERT(sequences[0].Length == 3);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 9);
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 0);

    CPPUNIT_ASSERT(sequences[1].Length == 5);
    CPPUNIT_ASSERT(sequences[1].HaystackOffset == 27);
    CPPUNIT_ASSERT(sequences[1].NeedleOffset == 1);

    CPPUNIT_ASSERT(sequences[2].Length == 5);
    CPPUNIT_ASSERT(sequences[2].HaystackOffset == 38);
    CPPUNIT_ASSERT(sequences[2].NeedleOffset == 2);
}

void StrategyTest::TestSeveralSequenceEntries()
{
    std::vector<SequenceInfo> sequences = FindNeedle(
        PATH_TO_TEST_FILES + L"TestStrategy\\several_sequence_entries_haystack.txt",
        PATH_TO_TEST_FILES + L"TestStrategy\\several_sequence_entries_needle.txt",
        3);

    CPPUNIT_ASSERT(4 == sequences.size());

    CPPUNIT_ASSERT(sequences[0].Length          == 4);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset  == 23);
    CPPUNIT_ASSERT(sequences[0].NeedleOffset    == 72);

    CPPUNIT_ASSERT(sequences[1].Length          == 7);
    CPPUNIT_ASSERT(sequences[1].HaystackOffset  == 27);
    CPPUNIT_ASSERT(sequences[1].NeedleOffset    == 10);

    CPPUNIT_ASSERT(sequences[2].Length          == 5);
    CPPUNIT_ASSERT(sequences[2].HaystackOffset  == 47);
    CPPUNIT_ASSERT(sequences[2].NeedleOffset    == 1);

    CPPUNIT_ASSERT(sequences[3].Length          == 6);
    CPPUNIT_ASSERT(sequences[3].HaystackOffset  == 68);
    CPPUNIT_ASSERT(sequences[3].NeedleOffset    == 0);
}

void StrategyTest::TestMinThreshold()
{
    std::wstring needlePath = PATH_TO_TEST_FILES + L"TestStrategy\\min_threshold_needle.txt";
    std::wstring haystackPath = PATH_TO_TEST_FILES + L"TestStrategy\\min_threshold_haystack.txt";

    {
        std::vector<SequenceInfo> sequences = FindNeedle(haystackPath, needlePath, 10);

        CPPUNIT_ASSERT(sequences.size() == 1);

        CPPUNIT_ASSERT(sequences[0].Length          == 10);
        CPPUNIT_ASSERT(sequences[0].HaystackOffset  == 65);
        CPPUNIT_ASSERT(sequences[0].NeedleOffset    == 0);
    }
    {
        std::vector<SequenceInfo> sequences = FindNeedle(haystackPath, needlePath, 9);

        CPPUNIT_ASSERT(2 == sequences.size());

        CPPUNIT_ASSERT(sequences[0].Length          == 9);
        CPPUNIT_ASSERT(sequences[0].HaystackOffset  == 54);
        CPPUNIT_ASSERT(sequences[0].NeedleOffset    == 0);

        CPPUNIT_ASSERT(sequences[1].Length          == 10);
        CPPUNIT_ASSERT(sequences[1].HaystackOffset  == 65);
        CPPUNIT_ASSERT(sequences[1].NeedleOffset    == 0);
    }

    {
        std::vector<SequenceInfo> sequences = FindNeedle(haystackPath, needlePath, 8);

        CPPUNIT_ASSERT(3 == sequences.size());

        CPPUNIT_ASSERT(sequences[0].Length          == 8);
        CPPUNIT_ASSERT(sequences[0].HaystackOffset  == 44);
        CPPUNIT_ASSERT(sequences[0].NeedleOffset    == 0);

        CPPUNIT_ASSERT(sequences[1].Length          == 9);
        CPPUNIT_ASSERT(sequences[1].HaystackOffset  == 54);
        CPPUNIT_ASSERT(sequences[1].NeedleOffset    == 0);

        CPPUNIT_ASSERT(sequences[2].Length          == 10);
        CPPUNIT_ASSERT(sequences[2].HaystackOffset  == 65);
        CPPUNIT_ASSERT(sequences[2].NeedleOffset    == 0);
    }
}

void StrategyTest::TestSame()
{
    std::wstring needlePath = PATH_TO_TEST_FILES + L"TestStrategy\\min_threshold_haystack.txt";
    std::wstring haystackPath = PATH_TO_TEST_FILES + L"TestStrategy\\min_threshold_haystack.txt";

    std::vector<SequenceInfo> sequences = FindNeedle(haystackPath, needlePath, 70);

    CPPUNIT_ASSERT(sequences.size() == 1);
    CPPUNIT_ASSERT(sequences[0].Length == 75);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 0);
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 0);
}

void StrategyTest::TestInputWrongFiles()
{
    std::unique_ptr<Callbacks> calls;

    CPPUNIT_ASSERT_THROW(Context context(L"lol", PATH_TO_TEST_FILES + L"TestStrategy\\min_threshold_haystack.txt", 0, std::move(calls)), std::invalid_argument);
    CPPUNIT_ASSERT_THROW(Context context(PATH_TO_TEST_FILES + L"TestStrategy\\min_threshold_haystack.txt", L"lol", 0, std::move(calls)), std::invalid_argument);
    CPPUNIT_ASSERT_THROW(Context context(L"lol", L"lol", 0, std::move(calls)), std::invalid_argument);
}

void StrategyTest::TestDoubleEntry()
{
    std::wstring needlePath = PATH_TO_TEST_FILES + L"TestStrategy\\double_entry_needle.txt";
    std::wstring haystackPath = PATH_TO_TEST_FILES + L"TestStrategy\\double_entry_haystack.txt";


    std::vector<SequenceInfo> sequences = FindNeedle(haystackPath, needlePath, 3);

    CPPUNIT_ASSERT(sequences.size() == 2);

    CPPUNIT_ASSERT(sequences[0].Length == 3);
    CPPUNIT_ASSERT(sequences[0].HaystackOffset == 5);
    CPPUNIT_ASSERT(sequences[0].NeedleOffset == 0);

    CPPUNIT_ASSERT(sequences[1].Length == 3);
    CPPUNIT_ASSERT(sequences[1].HaystackOffset == 8);
    CPPUNIT_ASSERT(sequences[1].NeedleOffset == 4);
}

void StrategyTest::TestFindBinaries()
{
    std::wcout << "\n" << GetTimestamp().c_str() << " started";
    {
        std::vector<SequenceInfo> sequences = FindNeedle(
            PATH_TO_TEST_FILES + L"TestBinary\\haystack.zip",
            PATH_TO_TEST_FILES + L"TestBinary\\VC_RED.cab",
            13);

        CPPUNIT_ASSERT(1 == sequences.size());
        CPPUNIT_ASSERT(0 == sequences[0].NeedleOffset);
        CPPUNIT_ASSERT(247984 == sequences[0].HaystackOffset);
        CPPUNIT_ASSERT(1927956 == sequences[0].Length);
    }
    std::wcout << "\n" << GetTimestamp().c_str() << " done";
    {
        std::vector<SequenceInfo> sequences = FindNeedle(
            PATH_TO_TEST_FILES + L"TestBinary\\haystack.zip",
            PATH_TO_TEST_FILES + L"TestBinary\\VC_RED.MSI",
            240000);

        CPPUNIT_ASSERT(1 == sequences.size());
        CPPUNIT_ASSERT(0 == sequences[0].NeedleOffset);
        CPPUNIT_ASSERT(40 == sequences[0].HaystackOffset);
        CPPUNIT_ASSERT(242176 == sequences[0].Length);
    }
    {
        std::vector<SequenceInfo> sequences = FindNeedle(
            PATH_TO_TEST_FILES + L"TestBinary\\haystack.zip",
            PATH_TO_TEST_FILES + L"TestBinary\\vcredist.bmp",
            3000);

        CPPUNIT_ASSERT(1 == sequences.size());

        CPPUNIT_ASSERT(5686 == sequences[0].Length);
        CPPUNIT_ASSERT(242258 == sequences[0].HaystackOffset);
        CPPUNIT_ASSERT(0 == sequences[0].NeedleOffset);
    }
    
}