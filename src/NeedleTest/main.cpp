#include "stdafx.h"
#include "NeedleInHaystack.h"
#include "StrategyTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(needleTest::StrategyTest);

class CustomProgressListener : public CppUnit::TextTestProgressListener
{
    void startTest(CppUnit::Test *test)
    {
        std::cout << "[" << test->getName().c_str() << "]";
    }

    void addFailure(const CppUnit::TestFailure& failure)
    {
        //std::cout << "[" << failure.failedTestName().c_str() << "]";
        std::cout << "\n-[FAILED]" << failure.thrownException()->message().details().c_str();
    }

    void endTest(CppUnit::Test *test)
    {
        std::cout << "\n";
        //std::cout << "\n----------------------------------\n";
    }
};

int _tmain(int argc, _TCHAR* argv[])
{
    CppUnit::TestResult controller;

    CppUnit::TestResultCollector result;
    controller.addListener(&result);

    CustomProgressListener progress;
    controller.addListener(&progress);

    CppUnit::TestRunner runner;
    runner.addTest(CppUnit::TestFactoryRegistry::getRegistry().makeTest());
    try
    {
        std::cout << "Running \n";
        runner.run(controller, "");

        std::cerr << std::endl;

        // Print test in a compiler compatible format.
        CppUnit::CompilerOutputter outputter(&result, std::cerr);

        outputter.write();
    }
    catch (std::invalid_argument &e)  // Test path not resolved
    {
        std::cerr << std::endl << "ERROR: " << e.what() << std::endl;
        return 0;
    }

    return result.wasSuccessful() ? 0 : 1;
}

