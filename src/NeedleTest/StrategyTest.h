#pragma once

namespace needleTest
{
    class StrategyTest : public CppUnit::TestFixture
    {
    public:
        void TestBeginOfFile();
        void TestEndOfFile();
        void TestFullNeedleSequence();
        void TestSecondEntry();
        void TestMiddleEntry();
        void TestSpecificaton();
        void TestSeveralSequenceEntries();
        void TestMinThreshold();        
        void TestSame();
        void TestInputWrongFiles();
        void TestDoubleEntry();
        void TestFindBinaries();

        CPPUNIT_TEST_SUITE(StrategyTest);
        
        CPPUNIT_TEST(TestSeveralSequenceEntries);
        CPPUNIT_TEST(TestSecondEntry);
        CPPUNIT_TEST(TestBeginOfFile);
        CPPUNIT_TEST(TestFullNeedleSequence);
        CPPUNIT_TEST(TestDoubleEntry);
        
        CPPUNIT_TEST(TestMinThreshold);
        
        CPPUNIT_TEST(TestEndOfFile);
        
        CPPUNIT_TEST(TestSame);
        CPPUNIT_TEST(TestSpecificaton);
        
        CPPUNIT_TEST(TestMiddleEntry);
        CPPUNIT_TEST(TestInputWrongFiles);

        //CPPUNIT_TEST(TestFindBinaries);

        CPPUNIT_TEST_SUITE_END();
    };
}